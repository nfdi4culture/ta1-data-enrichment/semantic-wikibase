# Semantic Wikibase (SWB) Docker Environment

Setup for the Docker environment to run a Semantic Wikibase (SWB) instance.
This project is based on the code of the project 'PhiWiki - a digital wiki for philosophy' https://gitlab.rlp.net/phiwiki/phiwiki_docker
It offers Example wiki pages and queries, a wikibase data insert from json file and a fast to-go setup process 
![SWB screenshot](images/swb_screenshot.jpg)

## Local Installation

### Preparations

#### Docker

The Docker docs provide straighforward installation instructions for
- [Linux](https://docs.docker.com/desktop/linux/install/),
- [macOS](https://docs.docker.com/desktop/mac/install/), and
- [Windows](https://docs.docker.com/desktop/windows/install/).

### Preparations: Wiki

#### Clone the Repository

First, clone the GitLab repository to your local host system by running

```bash
git clone https://gitlab.com/nfdi4culture/ta1-data-enrichment/semantic-wikibase.git
```

```bash
cd semantic-wikibase
```

### Add Sensitive Configuration Files

In order to access to enable the internal SSH exchange of the semantic wikibase application, you need various configuration files.

They should include:

- a `.env` file
- a folder named `ssh`, containing the following files
   - `authorized_keys`
   - `id_rsa`
   - `strict_exceptions.conf`

**Heads up:** None of these files should ever be checked in with version control.

Edit the file `.env.example` and save it as `.env` in the root folder of your local installation.
The files `authorized_keys` and `id_rsa` will be created from tthe setupssh script (see below)

#### Make the Shell Script Executable

_This step is relevant for users on Linux (including WSL2 on Windows) and Mac OS._

The shell script file: `swb` enables you to setup and modify the Docker application, make sure it can be executed:

```bash
chmod +x swb
```

### Setup SSH Files

We need to create a new ssh keypair in the folder `ssh` to enable communicatin between the docker containers.
The files in the `ssh` folder will be mounted into various containers. Because they configure SSH access to the containers, they need to have restricted access permissions to work properly. 
Run the following script (this will create the files `ssh/authorized_keys` and `ssh/id_rsa`):

```bash
sudo ./swb setupssh
```

The owner of these files will be set to `root` and the files might no longer be accessible to your default user account (unless you use `root`, of course).

#### Configure the Local Domains on Your Host System

In order to be able to access the local Wiki instance via the URL `http://swb.local` and the SPARQL endpoint via the URL `http://query.swb.local/sparql`, you need to add the following entry to your local hosts file:

```bash
127.0.0.1       swb.local
127.0.0.1       query.swb.local
```

You will find this file in different places in different OSs:

- Linux: /etc/hosts
- Mac: /etc/hosts
- Windows: \Windows\System32\drivers\etc\hosts

### Running the Docker Application

#### First Time Setup

When you start the swb Docker application for the first time, you will need to download data for the MediaWiki we use, install extensions, and do some configuration. 

But fear not, this is all automated. Just run

```bash
./swb setup
```

Now the application has been created and started you can access your local MedaWiki instance at http://phiwiki.local.

#### Stopping and Starting

Once the Docker application instance has been created, you can use `./swb stop` and `./swb start` to stop it and start it again. As long as you use these commands, it will always be the same instance.

In daily life, you will mostly need these commands. 

#### Removing and Assembling

In case you need to remove your local SWB instance completely, use `./swb remove`.

Your database data is still persisted in the `database` folder on your host system.

You might need to remove the Mediawiki extension folder manually before reinstall: `sudo rm -rf app/extensions/`


In case you want to create a new instance after removal that is still using the existing data and configuration, run `./swb assemble`.

Use these two commands if you add new components to the application or change  the `.env` file.

If you want to create a new instance with a blank database, remove the `database` folder and its content on your host and run [First Time Setup](#first-time-setup) again.

### Init Wiki

#### Insert Example Wiki pages and Data from dump
This is the easiest method to get a SWB installation with example pages and data

enter the command shell of the swb docker container and run the dump import script: 

```bash
docker exec -it swb_development_php_fpm bash
cd maintenance


```


#### Insert Wiki pages

- Open the Wiki in a browser (http://swb.local)
- Sign in using the credentials 'ADMIN_USER' and 'ADMIN_PASS' from `.env` file
- switch to the special page for import: http://swb.local/index.php?title=special:import
- Select the File `dumps/examples/examplePages.xml`
- Insert a prefix that suits your wiki name 
- Click the upload button
- navigate to the Mainpage http://swb.local

You should see a Page with links to example Pages and helpful resources

#### Insert data

The import script need at least python 3.10.x and you might need to install the used packages or setup a venv to install them

```bash
python python/dataInsert.py
```

#### Change wiki Appearance

Go to `Preferences` http://swb.local/index.php?title=Spezial:Einstellungen

Go to `Appearance`

Choose a skin. E.g. switch to `Tweeki` for a website-like Layout. Tweeki is based on bootstrap and optimized for Semantic Mediawiki 
( see: https://tweeki.kollabor.at/wiki/Welcome )



## Useful Resources

- [MediaWiki Setup](https://www.mediawiki.org/wiki/Manual:Config_script)
- [Semantic MediWiki Extension Setup](https://github.com/SemanticMediaWiki/SemanticMediaWiki/blob/a78acd623086790177ec2cbb5c0c83632f83541f/docs/INSTALL.md#installation-guide-brief)
- [Wikibase Setup](https://github.com/wikimedia/Wikibase/tree/REL1_35)
- [Semantic Wikibase Setup](https://github.com/ProfessionalWiki/SemanticWikibase/)
- [Page Forms Extension](https://www.mediawiki.org/wiki/Extension:Page_Forms)
- [Phiwiki Docker Setup](https://gitlab.rlp.net/phiwiki/phiwiki_docker)