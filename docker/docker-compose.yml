# swb Docker Compose setup
version: '3.4'

services:

  # Provisioning control service.
  swb_control:
    container_name: swb_${APP_ENV}_control
    depends_on:
      - swb_database
      - swb_php_fpm
      - swb_webserver
    env_file: ./../.env
    build:
      context: ./../
      dockerfile: docker/Dockerfile.control
    networks:
      - provisioning
    restart: ${CONTAINER_RESTART}
    tty: true
    volumes:
      - ./../ansible/ansible.cfg:/etc/ansible/ansible.cfg
      - ./../ansible/hosts.yml:/etc/ansible/hosts
      - ./../images:/var/tmp/images
      - ./../playbooks:/playbooks
      - ./../ssh/id_rsa:/root/.ssh/id_rsa
      - ./../ssh/strict_exceptions.conf:/etc/ssh/ssh_config.d/strict_exceptions.conf

  # Database service.
  swb_database:
    container_name: swb_${APP_ENV}_database
    environment:
      MARIADB_USER: ${DB_USER}
      MARIADB_PASSWORD: ${DB_PASS}
      MARIADB_DATABASE: ${DB_NAME}
      MARIADB_RANDOM_ROOT_PASSWORD: 'yes'
    image: mariadb:10.11
    networks:
      - database
    restart: ${CONTAINER_RESTART}
    volumes:
      - ./../database:/var/lib/mysql
      - ./../dumps:/var/tmp

  # MediaWiki service (including all extensions).
  swb_php_fpm:
    container_name: swb_${APP_ENV}_php_fpm
    depends_on:
      - swb_database
    env_file: ./../.env
    build:
      context: ./../
      dockerfile: docker/Dockerfile.php_fpm
    networks:
      - database
      - frontend
      - provisioning
    restart: ${CONTAINER_RESTART}
    volumes:
      - ./../app:/var/www/html
      - ./../composer.local.json:/var/www/html/composer.local.json
      - ./../ssh/authorized_keys:/root/.ssh/authorized_keys

  swb_webserver:
    container_name: swb_${APP_ENV}_webserver
    domainname: ${SERVER_NAME}
    depends_on:
      - swb_php_fpm
    env_file: ./../.env
    build:
      context: ./../
      dockerfile: docker/Dockerfile.webserver
    networks:
      - frontend
      - provisioning
    restart: ${CONTAINER_RESTART}
    ports:
      - ${PORT_HOST}:${PORT_WEBSERVER_CONTAINER}
    tty: true
    volumes:
      - ./../app:/var/www/html
      - ./../webserver/lighttpd.conf:/etc/lighttpd/lighttpd.conf
      - ./../webserver/lighttpd.users:/etc/lighttpd/lighttpd.users
      - ./../ssh/authorized_keys:/root/.ssh/authorized_keys

networks:
  database:
    name: "swb_${APP_ENV}_database_network"
  frontend:
    name: "swb_${APP_ENV}_frontend_network"
  provisioning:
    name: "swb_${APP_ENV}_provisioning_network"
